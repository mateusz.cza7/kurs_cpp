# Kompilator
CC = g++

# Flagi kompilatora
CFLAGS = -Wall -std=c++11

# Pliki źródłowe
SRCS =  src/ZadEnumExample.cpp
    src/ZadCarExample.cpp
    src/ZadStringExample.cpp
    src/ZadUnionExample.cpp
    src/ZadClassExample.cpp
    main.cpp

# Pliki nagłówkowe
HDRS =  src/ZadEnumExample.h
    src/ZadCarExample.h
    src/ZadStringExample.h
    src/ZadUnionExample.h
    src/ZadClassExample.h

# Nazwa pliku wykonywalnego
TARGET = ZadKcpp.cpp

# Reguła budowy
$(TARGET): $(SRCS) $(HDRS)
    $(CC) $(CFLAGS) -o $(TARGET) $(SRCS)

# Reguła czyszczenia
clean:
    rm -f $(TARGET)
