#include <iostream>
#include "ZadKcpp.h"

void showMenu() {
    std::cout << "Wybierz zadanie do uruchomienia:\n";
    std::cout << "1. ZadEnumExample\n";
    std::cout << "2. ZadCarExample\n";
    std::cout << "3. ZadStringExample\n";
    std::cout << "4. ZadClassExample\n";
    std::cout << "5. ZadUnionExample\n";
    std::cout << "6. Wyjście\n";
}

int main() {
    ZadKcpp zadKcpp;
    int choice;
    do {
        showMenu();
        std::cin >> choice;
        switch (choice) {
            case 1:
                zadKcpp.runZadEnumExample();
                break;
            case 2:
                zadKcpp.runZadCarExample();
                break;
            case 3:
                zadKcpp.runZadStringExample();
                break;
            case 4:
                zadKcpp.runZadClassExample();
                break;
            case 5:
                zadKcpp.runZadUnionExample();
                break;
            case 6:
                std::cout << "Wyjście...\n";
                break;
            default:
                std::cout << "Nieprawidłowy wybór, spróbuj ponownie.\n";
        }
    } while (choice != 6);

    return 0;
}
