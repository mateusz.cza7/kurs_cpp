#ifndef ZADCAREXAMPLE_H
#define ZADCAREXAMPLE_H

#include <string>

class Car {
public:
    Car();
    void printInfo();
private:
    std::string brand;
    std::string model;
    int year;
};

class ZadCarExample {
public:
    void demo();
};

#endif // ZADCAREXAMPLE_H
