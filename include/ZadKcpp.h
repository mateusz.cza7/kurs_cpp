#ifndef ZADKCPP_H
#define ZADKCPP_H

#include "ZadEnumExample.h"
#include "ZadCarExample.h"
#include "ZadStringExample.h"
#include "ZadUnionExample.h"
#include "ZadClassExample.h"

class ZadKcpp {
public:
    void runZadEnumExample();
    void runZadCarExample();
    void runZadStringExample();
    void runZadUnionExample();
    void runZadClassExample();
};

#endif // ZADKCPP_H
