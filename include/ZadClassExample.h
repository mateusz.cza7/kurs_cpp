#ifndef ZADCLASSEXAMPLE_H
#define ZADCLASSEXAMPLE_H

class MyClass {
public:
    MyClass(int a = 0, int b = 0, int c = 0);
    
    int getPublicVar() const;
    void setPublicVar(int value);
    
    int getProtectedVar() const;
    void setProtectedVar(int value);

    int getPrivateVar() const;
    void setPrivateVar(int value);
    
    bool isEqual(const MyClass& other) const;
    bool isNotEqual(const MyClass& other) const;

private:
    int publicVar;
protected:
    int protectedVar;
private:
    int privateVar;
};

class ZadClassExample {
public:
    void demo();
};

#endif // ZADCLASS_H
