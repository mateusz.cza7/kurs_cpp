#include "ZadClassExample.h"
#include <iostream>

MyClass::MyClass(int a, int b, int c) : publicVar(a), protectedVar(b), privateVar(c) {}


int MyClass::getPublicVar() const {
    return publicVar;
}

void MyClass::setPublicVar(int value) {
    publicVar = value;
}

int MyClass::getProtectedVar() const {
    return protectedVar;
}

void MyClass::setProtectedVar(int value) {
    protectedVar = value;
}

int MyClass::getPrivateVar() const {
    return privateVar;
}

void MyClass::setPrivateVar(int value) {
    privateVar = value;
}


bool MyClass::isEqual(const MyClass& other) const {
    return (publicVar == other.publicVar) && 
           (protectedVar == other.protectedVar) && 
           (privateVar == other.privateVar);
}

bool MyClass::isNotEqual(const MyClass& other) const {
    return !isEqual(other);
}

void ZadClassExample::demo() {
    MyClass obj1(1, 2, 3);
    MyClass obj2(1, 2, 3);
    MyClass obj3(4, 5, 6);

    std::cout << "Obj1 i Obj2 są " << (obj1.isEqual(obj2) ? "równe" : "różne") << std::endl;
    std::cout << "Obj1 i Obj3 są " << (obj1.isEqual(obj3) ? "równe" : "różne") << std::endl;

    obj3.setPublicVar(1);
    obj3.setProtectedVar(2);
    obj3.setPrivateVar(3);

    std::cout << "Po zmianach, Obj1 i Obj3 są " << (obj1.isEqual(obj3) ? "równe" : "różne") << std::endl;
}
