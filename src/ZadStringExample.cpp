#include "ZadStringExample.h"
#include <iostream>
#include <string>
/*
Zadanie 1.2
--------------------------------------
Napisać program (z zajęć) o treści zadanej w pliku LString.cc
- nazwać program: ZadStringExample.cc
*/
void ZadStringExample::run() {
    std::string str = "Hello";
    str += ", World!";
    std::cout << str << std::endl;
}
