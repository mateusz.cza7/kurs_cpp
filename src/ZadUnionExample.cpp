#include "ZadUnionExample.h"
#include <iostream>
#include <cstring>

/*
Zadanie 5.3
--------------------------------------
Pokaż działanie uni (niebezpieczenstwa, bledy)
- nazwij program: ZadUnia.cc
*/
union MyUnion {
    int intValue;
    float floatValue;
    char strValue[20];
};

void displayUnionContents(const MyUnion& u) {
    std::cout << "intValue: " << u.intValue << std::endl;
    std::cout << "floatValue: " << u.floatValue << std::endl;
    std::cout << "strValue: " << u.strValue << std::endl;
}

void ZadUnionExample::run() {
    MyUnion u;

    // Przykład z przypisaniem wartości intValue
    u.intValue = 42;
    std::cout << "Po przypisaniu intValue = 42:" << std::endl;
    displayUnionContents(u);

    // Przykład z przypisaniem wartości floatValue
    u.floatValue = 3.14f;
    std::cout << "\nPo przypisaniu floatValue = 3.14f:" << std::endl;
    displayUnionContents(u);

    // Przykład z przypisaniem wartości strValue
    std::strcpy(u.strValue, "Hello, World!");
    std::cout << "\nPo przypisaniu strValue = \"Hello, World!\":" << std::endl;
    displayUnionContents(u);

    // Demonstracja niebezpieczeństwa używania union
    std::cout << "\nDemonstracja niebezpieczeństwa używania union:" << std::endl;
    u.intValue = 65;
    std::cout << "Po przypisaniu intValue = 65:" << std::endl;
    displayUnionContents(u);

    std::cout << "\nPo przypisaniu strValue = \"A\": (brak zakończenia null)" << std::endl;
    std::strcpy(u.strValue, "A");
    std::cout << "char w postaci int: " << u.intValue << std::endl;
    std::cout << "char w postaci float: " << u.floatValue << std::endl;
}
