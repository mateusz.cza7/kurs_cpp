#include "ZadCarExample.h"
#include <iostream>
/*
Zadanie 1.1
--------------------------------------
Napisać prosty program z jedną klasą. Wszystko zawarte w jednym pliku: klasa oraz definicje funkcji. Definicje funkcji mogą być w klasie lub poza nią. Klasa oparta o przykład: KviCar.cc
- nazwać program: ZadClassExample.cc
*/

Car::Car() : brand("Volvo"), model("V60cc"), year(2019) {}

void Car::printInfo() {
    std::cout << "Brand: " << brand << ", Model: " << model << ", Year: " << year << "\n";
}

void ZadCarExample::demo() {
    Car myCar;
    myCar.printInfo();
}
