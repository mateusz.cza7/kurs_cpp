#include "ZadKcpp.h"
#include <iostream>

void ZadKcpp::runZadEnumExample() {
    ZadEnumExample example;
    example.run();
}

void ZadKcpp::runZadCarExample() {
    ZadCarExample example;
    example.demo();
}

void ZadKcpp::runZadStringExample() {
    ZadStringExample example;
    example.run();
}

void ZadKcpp::runZadUnionExample() {
    ZadUnionExample example;
    example.run();
}

void ZadKcpp::runZadClassExample() {
    ZadClassExample example;
    example.demo();
}
