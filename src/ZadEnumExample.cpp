#include "ZadEnumExample.h"
#include <iostream>
/*
Zadanie 1.0
--------------------------------------
Napisać krótki program oparty na pliku LTypeEnumerated.cc, zawierający przykład użycia typu enum (scoped lub unscoped, z miesiącami)
- nazwać program: ZadEnumExample.cc
*/
enum class Month {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};

void printMonth(Month month) {
    switch (month) {
        case Month::January: std::cout << "January"; break;
        case Month::February: std::cout << "February"; break;
        case Month::March: std::cout << "March"; break;
        case Month::April: std::cout << "April"; break;
        case Month::May: std::cout << "May"; break;
        case Month::June: std::cout << "June"; break;
        case Month::July: std::cout << "July"; break;
        case Month::August: std::cout << "August"; break;
        case Month::September: std::cout << "September"; break;
        case Month::October: std::cout << "October"; break;
        case Month::November: std::cout << "November"; break;
        case Month::December: std::cout << "December"; break;
    }
    std::cout << std::endl;
}

void ZadEnumExample::run() {
    for (int i = static_cast<int>(Month::January); i <= static_cast<int>(Month::December); ++i) {
        printMonth(static_cast<Month>(i));
    }
}
